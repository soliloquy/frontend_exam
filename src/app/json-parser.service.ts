import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class JsonParserService {

	private homeContent: any;
  constructor(private http: Http) { 
  }

  renderFile(file:string = '') :Observable<any>{
  	return this.http.get(file);
  }

  setHomeContent(val){
  	this.homeContent = val;
  }

  getHomeContent(){
  	return this.homeContent;
  }

}
