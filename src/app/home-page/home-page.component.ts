import { Component, OnInit } from '@angular/core';

import { JsonParserService } from '../json-parser.service';

import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
  providers: [JsonParserService]
})
export class HomePageComponent implements OnInit {

	content: any = ``;
	menu: Array<any> = [];
	name: string = '';
  constructor( private json: JsonParserService, private sanitizer: DomSanitizer ) { }

  ngOnInit() {
    this.json.renderFile("../assets/JSON/config/mainview.json").subscribe(data=>{
  	//this.json.renderFile("http://localhost:3000/mainview").subscribe(data=>{
  		let dt = data.json();
  		this.content = this.sanitizer.bypassSecurityTrustHtml(dt.content);
  		this.menu = dt.menu;
  		this.name = dt.name;
  	});

  }

}
