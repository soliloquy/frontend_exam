import { Component, OnInit } from '@angular/core';
import { JsonParserService } from '../json-parser.service';

@Component({
  selector: 'app-product-page',
  templateUrl: './product-page.component.html',
  styleUrls: ['./product-page.component.scss'],
  providers: [JsonParserService]
})
export class ProductPageComponent implements OnInit {

	name: string = "";
	title: string = "";
	type: string = "";
	cols : Array<any> = [];
	width : number = 100;
	dataSet: Array<any> = [];
	colKeys : Array<any> = [];
	dataKeys : Array<any> = [];
  constructor(private json: JsonParserService) { }

  ngOnInit() {
    this.json.renderFile("../assets/JSON/config/products.json").subscribe(data=>{
  	//this.json.renderFile("http://localhost:3000/config/products").subscribe(data=>{
  		let dt = data.json();
  		this.name = dt.name;
  		this.title = dt.title;
  		this.type = dt.type;
  		this.cols = dt.columndFields;
  		this.colKeys = Object.keys(this.cols);
  		this.width = Math.round(this.width/this.getVisibleCols());
  	});

    this.json.renderFile("../assets/JSON/data/products.json").subscribe(data=>{
  	//this.json.renderFile("http://localhost:3000/data/products").subscribe(data=>{
  		this.dataSet = data.json();
  		this.dataKeys = Object.keys(this.dataSet);
  		console.log(this.dataSet);
  	})
  }

  getVisibleCols():number{
  	let ret = 0;
  	this.cols.forEach((val,ind)=>{
  		if(val.isVisble) ++ret;
  	});
  	return ret;
  }

}
