import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';


import { AppComponent } from './app.component';
import { ProductPageComponent } from './product-page/product-page.component';
import { UserPageComponent } from './user-page/user-page.component';
import { HomePageComponent } from './home-page/home-page.component';

export const routes:Routes = [
  {
    path:"",
    component:HomePageComponent
  },
  {
    path:"users",
    component: UserPageComponent
  },
  {
    path:"products",
    component: ProductPageComponent
  }

];

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    UserPageComponent,
    ProductPageComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
