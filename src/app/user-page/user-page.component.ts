import { Component, OnInit } from '@angular/core';
import { JsonParserService } from '../json-parser.service';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.scss'],
  providers: [JsonParserService]
})
export class UserPageComponent implements OnInit {

	name: string = "";
	title: string = "";
	type: string = "";
	cols : Array<any> = [];
	width : number = 100;
	dataSet: Array<any> = [];
  constructor(private json: JsonParserService) { }

  ngOnInit() {
  	this.json.renderFile("../assets/JSON/config/users.json").subscribe(data=>{
  	//this.json.renderFile("http://localhost:3000/config/users").subscribe(data=>{
  		let dt = data.json();
  		this.name = dt.name;
  		this.title = dt.title;
  		this.type = dt.type;
  		this.cols = dt.columndFields;
  		this.width = Math.round(this.width/this.getVisibleCols());
  	});

  	this.json.renderFile("../assets/JSON/data/users.json").subscribe(data=>{
  	//this.json.renderFile("http://localhost:3000/data/users").subscribe(data=>{
  		this.dataSet = data.json();
  		console.log(this.dataSet);
  	})
  }

  getVisibleCols():number{
  	let ret = 0;
  	this.cols.forEach((val,ind)=>{
  		if(val.isVisble) ++ret;
  	});
  	return ret;
  }

}
