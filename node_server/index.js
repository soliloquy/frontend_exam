var cors = require('cors');
var express = require("express"); 
var app = express();
var fs = require('fs');

app.use(cors());
app.get("/mainview", function(req, res)  { 
	var obj;
	fs.readFile('JSON/config/mainview.json',(err,data)=>{
		if(err) throw err;    
	    obj = JSON.parse(data);
    	res.json(obj);
	});

});

app.get("/config/products", function(req, res)  { 
	var obj;
	fs.readFile('JSON/config/products.json',(err,data)=>{
		if(err) throw err;    
	    obj = JSON.parse(data);
    	res.json(obj);
	}); 

});

app.get("/config/users", function(req, res)  { 
	var obj;
	fs.readFile('JSON/config/users.json',(err,data)=>{
		if(err) throw err;    
	    obj = JSON.parse(data);
    	res.json(obj);
	});

});

app.get("/data/products", function(req, res)  { 
	var obj;
	fs.readFile('JSON/data/products.json',(err,data)=>{
		if(err) throw err;    
	    obj = JSON.parse(data);
    	res.json(obj);
	}); 

});

app.get("/data/users", function(req, res)  { 
	var obj;
	fs.readFile('JSON/data/users.json',(err,data)=>{
		if(err) throw err;    
	    obj = JSON.parse(data);
    	res.json(obj);
	}); 

});


app.listen(3000, function() {  
    console.log("Node server running..");
});

module.exports = app;